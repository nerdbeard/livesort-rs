//! Responsible for reading lines from a series of files

use std::{
    fs::OpenOptions,
    io::{self, BufRead, BufReader},
    sync::{
        atomic::{AtomicBool, Ordering::Relaxed},
        mpsc::SyncSender,
        Arc,
    },
    thread::{self, spawn},
};

use crate::event::{EventHandler, SourceEvent};

pub struct SourceReader {
    sources: Vec<String>,
    quitting: Arc<AtomicBool>,
    channel: SyncSender<Box<dyn EventHandler + Send>>,
}

impl SourceReader {
    pub fn new(
        sources: &[&str],
        quitting: Arc<AtomicBool>,
        channel: SyncSender<Box<dyn EventHandler + Send>>,
    ) -> Self {
        Self {
            sources: sources.iter().map(|path| path.to_string()).collect(),
            quitting,
            channel,
        }
    }
    pub fn run(self) -> thread::JoinHandle<io::Result<()>> {
        spawn(move || self.process())
    }
    fn process(self) -> io::Result<()> {
        let mut buf = String::new();
        'source: for path in self.sources {
            {
                let mut source = BufReader::new(OpenOptions::new().read(true).open(&path)?);
                while !self.quitting.load(Relaxed)
                    && (match source.read_line(&mut buf) {
                        Ok(bytes) => bytes,
                        Err(e) => {
                            self.channel
                                .send(Box::new(SourceEvent::ReadError {
                                    source: path,
                                    error: e,
                                }))
                                .expect("Receiver hung up");
                            // This source had a read error; break out
                            // of while loop and back to for loop to
                            // get a fresh one
                            continue 'source;
                        }
                    }) > 0
                {
                    let line = String::from(
                        // Remove line ending.  XXX -- Don't remove other
                        // trailing whitespace!!
                        buf.trim_end(),
                    );
                    self.channel
                        .send(Box::new(SourceEvent::LineRead { line }))
                        .unwrap_or(());
                    buf.truncate(0);
                }
                self.channel
                    .send(Box::new(SourceEvent::SourceExhausted {
                        source: path.to_string(),
                    }))
                    .unwrap_or(());
            };
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::sync::mpsc::sync_channel;

    use super::*;

    #[test]
    fn test() {
        let (sender, receiver) = sync_channel(0);
        let reader = SourceReader::new(&["/dev/null"], Arc::new(AtomicBool::new(true)), sender);
        let handle = reader.run();
        receiver.recv().unwrap();
        handle.join().unwrap().unwrap();
    }
}
