//! Responsible for backing store and status line

use std::fs::File;
use std::io::{Result, Write};

use crate::complain;
use crate::term::TermBase;

pub type Display = DisplayBase<File>;

pub struct DisplayBase<TtyOutT: Write>
where
    TtyOutT: Write,
{
    lines: Vec<String>,
    width: usize,
    pub term: TermBase<TtyOutT>,
    status_line: String,
}

impl<TtyOutT> DisplayBase<TtyOutT>
where
    TtyOutT: Write,
{
    pub fn new(width: usize, height: usize, mut term: TermBase<TtyOutT>) -> Self {
        term.grab()
            .unwrap_or_else(|err| complain(&format!("Couldn't grab screen: {}", err)));
        Self {
            lines: vec![String::new(); height],
            width,
            term,
            status_line: String::new(),
        }
    }

    fn insert_line(&mut self, line: String, index: u16) -> Result<()> {
        debug_assert!(
            !(line.contains('\n') || line.contains('\r')),
            "Newlines not allowed"
        );
        self.term.insert_line(line.as_bytes(), index)?;
        let index: usize = index.into();
        self.lines[index..].rotate_right(1);
        self.lines[index] = line;
        Ok(())
    }

    fn delete_line(&mut self, index: u16) -> Result<()> {
        self.lines[index as usize..].rotate_left(1);
        let len = self.lines.len();
        self.lines[len - 1] = "".into();
        self.term.delete_line(index)
    }

    pub fn scroll_down(&mut self, line: String) -> Result<()> {
        self.delete_line(0)?;
        self.insert_line(line, (self.lines.len() - 1).try_into().unwrap())
    }

    pub fn scroll_up(&mut self, line: String) -> Result<()> {
        self.insert_line(line, 0)
    }

    pub fn update_display(&mut self, new_lines: Vec<String>) -> Result<()> {
        for (i, line) in new_lines.into_iter().enumerate() {
            if self.lines[i] != line {
                self.insert_line(line, i.try_into().unwrap())?;
            }
        }
        self.term.flush()
    }

    /// Used when the user requests a redraw or the entire screen
    /// contents have scrolled off the display
    pub fn redraw(&mut self) -> Result<()> {
        self.term.clear()?;
        self.term.write(self.lines.join("\n").as_bytes())?;
        self.term.flush()
    }

    pub fn set_status_line(&mut self, mut status_line: String) -> Result<()> {
        status_line.truncate(self.width);

        if self.status_line == status_line {
            return Ok(());
        }

        // Erase the old status line
        {
            let len = self.status_line.len();
            if len > status_line.len() {
                self.term.move_cursor(
                    (self.lines.len() - 1).try_into().unwrap(),
                    (self.width - len).try_into().unwrap(),
                )?;
                self.term.kill_to_eol()?;
            }
        }

        self.status_line = status_line;
        self.redraw_status_line()
    }

    pub fn redraw_status_line(&mut self) -> Result<()> {
        self.term.move_cursor(
            (self.lines.len() - 1).try_into().unwrap(),
            (self.width - self.status_line.len()).try_into().unwrap(),
        )?;
        self.term.set_reverse_video(true)?;
        self.term.write(self.status_line.as_bytes())?;
        self.term.set_reverse_video(false)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::term::tests::make_test_term;

    type TestDisplay = DisplayBase<Vec<u8>>;

    #[test]
    fn new() {
        let display = TestDisplay::new(80, 24, make_test_term());
        assert_eq!(display.lines, vec![""; 24]);
        assert_eq!(display.width, 80);
        assert!(display.term.grabbed());
    }

    #[test]
    fn insert_line() -> Result<()> {
        let mut display = TestDisplay::new(3, 3, make_test_term());
        let line = String::from("a line");
        display.insert_line(line, 1)?;
        assert_eq!(display.lines, vec!["", "a line", ""]);
        Ok(())
    }

    #[test]
    fn update_display() -> Result<()> {
        let mut display: TestDisplay = TestDisplay::new(3, 3, make_test_term());
        let new_lines = vec![
            String::from(""),
            String::from("a line"),
            String::from("another line"),
        ];
        display.update_display(new_lines)?;
        assert_eq!(display.lines, vec!["", "a line", "another line"]);
        let new_lines = vec![String::from(""), String::from(""), String::from("a line")];
        display.update_display(new_lines)?;
        assert_eq!(display.lines, vec!["", "", "a line"]);
        Ok(())
    }

    fn setup_for_scroll() -> TestDisplay {
        let mut display = TestDisplay::new(3, 3, make_test_term());
        let new_lines = vec![String::from("1"), String::from("2"), String::from("3")];
        display.update_display(new_lines).unwrap();
        display
    }

    #[test]
    fn scroll_down() -> Result<()> {
        let mut display = setup_for_scroll();
        display.scroll_down(String::from("down"))?;
        assert_eq!(display.lines, vec!["2", "3", "down"]);
        Ok(())
    }

    #[test]
    fn scroll_up() -> Result<()> {
        let mut display = setup_for_scroll();
        display.scroll_up(String::from("up"))?;
        assert_eq!(display.lines, vec!["up", "1", "2"]);
        Ok(())
    }
}
