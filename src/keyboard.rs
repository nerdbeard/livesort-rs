//! Start a thread that reads keys and sends corresponding events back
//! to the event loop

use std::{
    fs::File,
    io::{self, Read},
    os::fd::AsRawFd,
    sync::{
        atomic::{AtomicBool, Ordering::Relaxed},
        mpsc::SyncSender,
        Arc,
    },
    thread::{self, JoinHandle},
};

use termios::Termios;

use crate::{
    complain,
    event::{EventHandler, InputEvent},
};

/// Configure tty input to not echo on read, and to wait for a tenth
/// of a second for each character when reading
fn no_echo(fileno: i32) -> io::Result<Termios> {
    let mut term = Termios::from_fd(fileno)?;
    let saved_termios = term; // implicit clone
    term.c_lflag &= !(termios::ECHO | termios::ICANON);
    term.c_cc[termios::VTIME] = 1; // One "decisecond"
    term.c_cc[termios::VMIN] = 0; // 0 means time out if no characters
                                  // received in VTIME
    termios::tcsetattr(fileno, termios::TCSANOW, &term).unwrap_or_else(|e| {
        complain(&format!("Error setting terminal parameters: {}", e));
    });
    Ok(saved_termios)
}

/// Restore saved terminal input settings
fn restore_termios(fileno: i32, termios: Termios) -> io::Result<()> {
    termios::tcsetattr(fileno, termios::TCSANOW, &termios)
}

/// Read one Optional character of user input if available; None
/// indicates read timed out after 0.1 seconds
pub fn poll(tty: &mut dyn Read) -> io::Result<Option<InputEvent>> {
    let mut buf: Vec<u8> = vec![0];
    let len = tty.read(&mut buf)?;
    match len {
        0 => Ok(None),
        1 => Ok(Some(InputEvent::from(buf[0]))),
        _ => {
            unreachable!("Strange value from read(&{:?}) -> {}", buf, len);
        }
    }
}

/// Pollllop
pub fn llop(
    tty_path: &str,
    until: Arc<AtomicBool>,
    channel: SyncSender<Box<dyn EventHandler + Send>>,
) -> io::Result<()> {
    let mut tty = std::fs::OpenOptions::new().read(true).open(tty_path)?;
    let term = no_echo(tty.as_raw_fd())?;
    let restore = |tty: &File| restore_termios(tty.as_raw_fd(), term);

    loop {
        if until.load(Relaxed) {
            break;
        }
        match poll(&mut tty) {
            Err(e) => {
                restore(&tty)?;
                panic!("Error reading keyboard: {:?}", e);
            }
            Ok(option) => {
                if let Some(c) = option {
                    if channel.send(Box::new(c)).is_err() {
                        break; // Receiver has hung up; no point in
                               // continuing
                    }
                }
            }
        };
    }
    restore(&tty)
}

/// Start polllloping
pub fn start_llop(
    tty_path: String,
    until: Arc<AtomicBool>,
    channel: SyncSender<Box<dyn EventHandler + Send>>,
) -> JoinHandle<io::Result<()>> {
    thread::Builder::new()
        .name(String::from("keyboard"))
        .spawn(move || llop(&tty_path, until, channel))
        .unwrap()
}

#[cfg(test)]
mod tests {
    use std::sync::mpsc::sync_channel;

    use super::*;

    #[test]
    fn test() {
        let tty_path = String::from("/dev/tty");
        let until = Arc::new(AtomicBool::new(false));
        let (sender, _receiver) = sync_channel(0);
        let handle: JoinHandle<io::Result<()>> = start_llop(tty_path, until.clone(), sender);
        until.store(true, Relaxed);
        handle.join().unwrap().unwrap();
    }
}
