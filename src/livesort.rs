use crate::{complain, config, display, event::EventHandler, index, keyboard, term};

use std::{
    collections::BTreeSet,
    fs::{File, OpenOptions},
    io::{self, Write},
    path::Path,
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc::sync_channel,
        Arc,
    },
};

/**
Live file handles, buffers, and intermediate sorted values bound to a
[Config].
 */
pub struct LiveSort {
    config: config::Config,
    /// The indexed data
    pub lines: Box<dyn index::Index>,
    /// Write to the pipeline
    stdout: Box<dyn Write>,
    pub height: usize,
    width: usize,
    pub display: display::Display,
    tty_in_path: String,
    pub cursor_line: usize,
    pub active_sources: BTreeSet<String>,
    pub quitting: Arc<AtomicBool>,
}

/**
Ask POSIX how big our terminal window is, presently.
 */
fn get_win_size() -> io::Result<(usize, usize)> {
    nix::ioctl_read_bad!(
        inner_get_win_size,
        nix::libc::TIOCGWINSZ,
        nix::libc::winsize
    );
    let term_fd = nix::fcntl::open(
        "/dev/tty",
        nix::fcntl::OFlag::O_RDONLY,
        nix::sys::stat::Mode::S_IRUSR,
    )?;
    let mut winsize = nix::libc::winsize {
        ws_col: 0,
        ws_row: 0,
        ws_xpixel: 0,
        ws_ypixel: 0,
    };
    unsafe { inner_get_win_size(term_fd, &mut winsize) }?;
    Ok((winsize.ws_col.into(), winsize.ws_row.into()))
}

impl LiveSort {
    pub fn new(
        config: config::Config,
        stdout: Box<dyn Write>,
        terminfo: terminfo::Database,
    ) -> io::Result<Self> {
        let (width, height) = get_win_size().unwrap_or_else(|e| {
            complain(&format!("Failed to get window size: {}", e));
            (80, 24)
        });
        let tty_out = Self::open_tty(&config)?;
        let lines = index::factory(config.key, config.reverse);
        let tty_in_path = String::from(if config.notty {
            "/dev/stdout"
        } else {
            "/dev/tty"
        });
        let mut active_sources = BTreeSet::new();
        for source in config.sources.iter() {
            active_sources.insert(source.clone());
        }
        Ok(Self {
            config,
            lines,
            stdout,
            height,
            width,
            display: display::Display::new(width, height, term::Term::new(tty_out, terminfo)),
            tty_in_path,
            cursor_line: 0,
            active_sources,
            quitting: Arc::new(AtomicBool::new(false)),
        })
    }

    fn open_tty(config: &config::Config) -> io::Result<File> {
        Ok(if config.notty {
            OpenOptions::new()
                .write(true)
                .open(Path::new("/dev/stdout"))?
        } else {
            OpenOptions::new()
                .write(true)
                .open(Path::new("/dev/tty"))
                .or_else(|err| {
                    complain(&format!(
                        "Couldn't open /dev/tty for writing: {}; using /dev/stdout",
                        err,
                    ));
                    OpenOptions::new()
                        .write(true)
                        .open(Path::new("/dev/stdout"))
                })?
        })
    }

    pub fn run(&mut self) -> io::Result<()> {
        // Set up a signal hander with ctrlc that signals this thread
        // when an interrupt has been received
        let quitting = self.quitting.clone();
        ctrlc::set_handler(move || {
            if quitting.load(Ordering::Relaxed) {
                panic!("Received two interrupts")
            } else {
                quitting.store(true, Ordering::Relaxed)
            }
        })
        .expect("Couldn't set interupt handler");

        self.display.term.set_cursor_visible(false)?;

        let mut result = Ok(());

        let (sender, receiver) = sync_channel::<Box<dyn EventHandler + Send>>(0);

        let sources: Vec<&str> = self.config.sources.iter().map(String::as_str).collect();
        let sources =
            crate::source::SourceReader::new(&sources, self.quitting.clone(), sender.clone());
        let sources = sources.run();

        let keyboard =
            keyboard::start_llop(self.tty_in_path.clone(), self.quitting.clone(), sender);

        // Main event loop
        for event in receiver {
            if self.quitting.load(Ordering::Relaxed) {
                break;
            };
            result = event.handle(self);
            if result.is_err() {
                // Signal to the other threads to quit as well
                self.quitting.store(true, Ordering::Relaxed);
                break;
            }
        }

        // Restore the TTY input settings and reap the keyboard
        // thread.  Join will take max one "decisecond" for tty thread
        // to time out its read
        match keyboard.join() {
            Ok(inner) => match inner {
                Ok(()) => {}
                Err(e) => {
                    complain(&format!("Error from kbd thread: {}", e));
                }
            },
            Err(_) => {
                complain("Panic in kbd thread");
            }
        }

        // Join the sources thread to report any error it sent.  Not
        // strictly necessary I think.
        match sources.join() {
            Ok(inner) => match inner {
                Ok(()) => (),
                Err(err) => complain(&format!("Error from sources thread: {}", err)),
            },
            Err(_) => {
                complain("Panic in sources thread");
            }
        };

        // Display and Term should go out of scope at this point, but
        // just release the Term for now
        self.display
            .term
            .release()
            .unwrap_or_else(|e| complain(&format!("Error releasing terminal: {}", e)));

        self.output_results()
            .unwrap_or_else(|e| complain(&format!("Error outputting results: {}", e)));

        if let Result::Err(ref err) = result {
            complain(&format!("error processing: {}; processing halted", err));
        }
        result
    }

    fn output_results(&mut self) -> io::Result<()> {
        let mut lines = self.lines.get_all();
        lines.try_for_each(|line| self.stdout.write_all(format!("{}\n", line).as_bytes()))
    }

    fn crop_line<'a>(&'a self, line: &'a str) -> &str {
        &line[..{ line.len().min(self.width) }]
    }

    pub fn calculate_display(&self) -> Vec<String> {
        self.lines
            .get_range(self.cursor_line, self.height)
            .map(|s| String::from(self.crop_line(s)))
            .collect()
    }

    pub fn cursor_line(&self) -> usize {
        self.cursor_line
    }

    pub fn set_cursor_line(&mut self, line_no: usize) -> io::Result<()> {
        let line_no = line_no.min(self.lines.count());

        if line_no == self.cursor_line {
            return Ok(());
        }

        // Clear the status line so it doesn't leave streaks
        self.display.set_status_line(String::from(""))?;
        {
            let delta: isize = line_no as isize - self.cursor_line as isize;
            let dirty = delta.abs();
            let height = self.height.try_into().unwrap();
            if dirty < height {
                // The display is only partially dirty, scroll and paint.
                while self.cursor_line != line_no {
                    if delta > 0 {
                        self.scroll_down()?;
                    } else {
                        self.scroll_up()?;
                    }
                }
            } else {
                // The entire screen is dirty, print it all
                self.cursor_line = line_no;
                self.refresh_display()?;
            }
        }
        // Replace the status line with the updated cursor location
        self.update_status_line()
    }

    fn scroll_down(&mut self) -> io::Result<()> {
        self.cursor_line += 1;
        self.display.scroll_down(String::from(
            self.crop_line(
                self.lines
                    .get_range(self.cursor_line + self.height - 1, 1)
                    .next()
                    .unwrap(),
            ),
        ))
    }

    fn scroll_up(&mut self) -> io::Result<()> {
        self.cursor_line -= 1;
        self.display.scroll_up(String::from(
            self.crop_line(self.lines.get_range(self.cursor_line, 1).next().unwrap()),
        ))
    }

    /// Repaint the entire screen.
    pub fn refresh_display(&mut self) -> io::Result<()> {
        self.display.redraw()
    }

    pub fn update_status_line(&mut self) -> io::Result<()> {
        self.display.set_status_line(format!(
            "  Lines {}-{} of {}  ",
            self.cursor_line + 1,
            self.cursor_line + self.height,
            self.lines.count(),
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn fixture_config(reverse: bool) -> config::Config {
        config::Config {
            sources: vec![],
            key: config::SortKey::Lexical,
            reverse,
            notty: false,
        }
    }

    fn fixture_state(config: config::Config) -> LiveSort {
        let height: usize = 24;
        let width: usize = 80;
        let lines = index::factory(config.key, config.reverse);

        LiveSort {
            config,
            lines,
            stdout: Box::new(std::io::sink()),
            height,
            width,
            display: display::Display::new(
                width,
                height,
                // I want to replace this with a test Ter mbut doing
                // that would require exposing generics on Display so
                // I guess this is broken for now
                term::Term::new(
                    File::open("/dev/null").unwrap(),
                    terminfo::Database::from_name("dumb").unwrap(),
                ),
            ),
            tty_in_path: String::from("/dev/zero"),
            cursor_line: 0,
            quitting: Arc::new(AtomicBool::new(false)),
            active_sources: BTreeSet::new(),
        }
    }

    // I could have sworn there were tests here once.

    #[test]
    fn calculate_display() {
        fixture_state(fixture_config(false)).calculate_display();
    }
}
