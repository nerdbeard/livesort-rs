//! Responsible for terminal output

//! - Write terminal control codes and text to the terminal
//! - Controls:
//!   - cursor visibility
//!   - inverse video state
//! - Restore the terminal how we found it
//!   - ...automatically when dropped

use std::fs::File;
use std::io::{Result, Write};

use terminfo::capability as caps;
use terminfo::expand;

use crate::complain;

macro_rules! write_tcap {
    ( $self:expr, $cap:ty ) => {{
        let code = $self.terminfo_code::<$cap>(&[]);
        $self.write(&code)?;
    }};
    ( $self:expr, $cap:ty, $p1:expr ) => {{
        let code = $self.terminfo_code::<$cap>(&[$p1]);
        $self.write(&code)?;
    }};
    ( $self:expr, $cap:ty, $p1:expr, $p2:expr ) => {{
        let code = $self.terminfo_code::<$cap>(&[$p1, $p2]);
        $self.write(&code)?;
    }};
}

pub type Term = TermBase<File>;

pub struct TermBase<TtyOutT>
where
    TtyOutT: Write,
{
    tty_out: TtyOutT,
    terminfo: terminfo::Database,
    grabbed: bool,
    cursor_visible: bool,
    reverse_video: bool,
}

impl<TtyOutT> TermBase<TtyOutT>
where
    TtyOutT: Write,
{
    pub fn new(tty_out: TtyOutT, terminfo: terminfo::Database) -> Self {
        Self {
            tty_out,
            terminfo,
            grabbed: false,
            cursor_visible: true,
            reverse_video: false,
        }
    }

    pub fn write(&mut self, output: &[u8]) -> Result<()> {
        self.tty_out.write_all(output)
    }

    pub fn flush(&mut self) -> Result<()> {
        self.tty_out.flush()
    }

    fn terminfo_code<'a, Cap>(&'a mut self, params: &[u16]) -> Vec<u8>
    where
        Cap: terminfo::Capability<'a>,
    {
        match self.terminfo.get::<Cap>() {
            Some(cap) => match cap.into().unwrap() {
                caps::Value::String(cap_str) => {
                    match params.len() {
                        1 => {
                            expand!(cap_str; params[0])
                        }
                        2 => {
                            expand!(cap_str; params[0], params[1])
                        }
                        3 => {
                            expand!(cap_str; params[0], params[1], params[2])
                        }
                        4 => {
                            expand!(cap_str; params[0], params[1], params[2], params[3])
                        }
                        _ => {
                            expand!(cap_str)
                        }
                    }
                }
                .unwrap(),
                _ => b"???".to_vec(),
            },
            None => format!("[{} {:?}]", Cap::name(), params)
                .as_bytes()
                .to_vec(),
        }
    }

    pub fn grab(&mut self) -> Result<()> {
        if self.grabbed {
            return Ok(());
        }

        write_tcap!(self, caps::EnterCaMode);
        self.grabbed = true;

        Ok(())
    }

    pub fn release(&mut self) -> Result<()> {
        if !self.grabbed {
            return Ok(());
        }

        self.set_cursor_visible(true)
            .unwrap_or_else(|e| complain(&format!("Failed to set cursor visibility: {}", e)));
        self.set_reverse_video(false)
            .unwrap_or_else(|e| complain(&format!("Failed to reset reverse video: {}", e)));

        write_tcap!(self, caps::ExitCaMode);

        self.grabbed = false;

        Ok(())
    }

    #[cfg(test)]
    pub fn grabbed(&self) -> bool {
        self.grabbed
    }

    pub fn move_cursor(&mut self, row: u16, col: u16) -> Result<()> {
        write_tcap!(self, caps::CursorAddress, row, col);
        Ok(())
    }

    pub fn insert_line(&mut self, line: &[u8], index: u16) -> Result<()> {
        self.move_cursor(index, 0)?;
        write_tcap!(self, caps::InsertLine);
        self.write(line)
    }

    pub fn delete_line(&mut self, index: u16) -> Result<()> {
        self.move_cursor(index, 0)?;
        write_tcap!(self, caps::DeleteLine);
        Ok(())
    }

    pub fn clear(&mut self) -> Result<()> {
        write_tcap!(self, caps::ClearScreen);
        Ok(())
    }

    pub fn kill_to_eol(&mut self) -> Result<()> {
        write_tcap!(self, caps::ClrEol);
        Ok(())
    }

    pub fn set_cursor_visible(&mut self, cursor_visible: bool) -> Result<()> {
        if self.cursor_visible == cursor_visible {
            return Ok(());
        }

        if cursor_visible {
            write_tcap!(self, caps::CursorVisible);
        } else {
            write_tcap!(self, caps::CursorInvisible);
        }

        self.cursor_visible = cursor_visible;

        Ok(())
    }

    pub fn set_reverse_video(&mut self, inverse_video: bool) -> Result<()> {
        if self.reverse_video == inverse_video {
            return Ok(());
        }

        if inverse_video {
            write_tcap!(self, caps::EnterReverseMode);
        } else {
            write_tcap!(self, caps::ExitAttributeMode);
        }

        self.reverse_video = inverse_video;
        Ok(())
    }
}

impl<TtyOutT> Drop for TermBase<TtyOutT>
where
    TtyOutT: Write,
{
    fn drop(&mut self) {
        self.release().unwrap_or(());
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    pub type TestTerm = TermBase<Vec<u8>>;
    pub fn make_test_term() -> TestTerm {
        TermBase::new(
            Vec::<u8>::new(),
            terminfo::Database::from_name("dumb").unwrap(),
        )
    }

    macro_rules! term_test {
        ( $name:ident, $inner:block ) => {
            #[test]
            fn $name() -> Result<()> {
                $inner;
                Ok(())
            }
        };
    }

    term_test!(test_new, {
        let term = make_test_term();
        assert!(!term.grabbed);
        assert_eq!(term.tty_out, &[]);
    });

    // I don't know how to test the parts that require a live
    // terminal.  I think I can use nix::pty::openpty to create a
    // pseudoterminal but for now I have opted to just allow it to
    // degrade instead of fail.  I think it should be an error to be
    // unable to configure the terminal.
    term_test!(test_grab, {
        let mut term = make_test_term();
        term.grab()?;
        assert!(term.grabbed);
        assert_ne!(term.tty_out, &[]);
    });

    term_test!(test_release, {
        let mut term = make_test_term();
        term.grabbed = true;
        term.reverse_video = true;
        term.cursor_visible = false;
        term.release()?;
        assert!(!term.grabbed);
        assert!(!term.reverse_video);
        assert!(term.cursor_visible);
        assert_ne!(term.tty_out, &[]);
    });

    term_test!(test_write, {
        let mut term = make_test_term();
        let message = b"message";
        term.write(message)?;
        assert_eq!(term.tty_out, message);
    });

    term_test!(test_cursor_visible, {
        let mut term = make_test_term();
        assert!(term.cursor_visible);
        term.set_cursor_visible(false)?;
        assert!(!term.cursor_visible);
        term.set_cursor_visible(true)?;
        assert!(term.cursor_visible);
        assert_eq!(
            String::from_utf8(term.tty_out.to_vec()).unwrap(),
            "[cursor_invisible []][cursor_visible []]"
        );
    });

    term_test!(test_terminfo_code, {
        let mut term = make_test_term();
        assert_eq!(term.terminfo_code::<terminfo::capability::Bell>(&[]), b"");
        let result = String::from_utf8(
            term.terminfo_code::<terminfo::capability::CursorAddress>(&[123, 321]),
        )
        .unwrap();
        assert!(
            result.as_str().contains("123") && result.as_str().contains("321"),
            "result: {:?}",
            result
        );
    });

    term_test!(test_drop, {
        let mut buf = vec![];
        {
            let mut term = TermBase::new(&mut buf, terminfo::Database::from_name("xterm").unwrap());
            term.grabbed = true;
        }
        assert_ne!(String::from_utf8(buf).unwrap(), "");
    });
}
