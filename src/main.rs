/*!
# ``livesort-rs``

Read input from stdin, output error to stderr, and output sorted
output to stdout; like ``/bin/sort``.  Meanwhile, open
``/dev/tty`` directly and use it to present a UI with live
paginated sort results as they become available.
*/

use std::io::{stdout, Result};

use config::{cli, Config};
use livesort::LiveSort;
use terminfo::Database;

mod config;
mod display;
mod event;
mod index;
mod keyboard;
mod livesort;
mod source;
mod term;

fn complain(msg: &str) {
    eprintln!("livesort: {}", msg);
}

fn main() -> Result<()> {
    let config = Config::from(cli().get_matches());
    let database = Database::from_env()
        .or_else(|e| {
            complain(&format!("{} -- Falling back to dumb term", e));
            Database::from_name("dumb")
        })
        .unwrap();
    let mut state: LiveSort = LiveSort::new(config, Box::new(stdout()), database)?;
    state.run()
}
