use clap::{arg, command, ArgMatches, Command};

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum SortKey {
    Lexical,
    Numeric,
}

pub struct Config {
    pub sources: Vec<String>,
    pub reverse: bool,
    pub key: SortKey,
    pub notty: bool,
}

pub fn cli() -> Command {
    command!().args(&[
        arg!(-r --reverse "Show results in descending order"),
        arg!(-n --numeric "Show results in numeric order (instead of lexical)"),
        arg!(--notty "Don't re-open /dev/tty; used for development testing"),
        arg!([source] ... "Path to read unsorted lines from"),
    ])
}

impl From<ArgMatches> for Config {
    fn from(matches: ArgMatches) -> Self {
        Config {
            sources: matches
                .get_many::<String>("source")
                .map(|v| v.map(String::clone).collect())
                .unwrap_or_else(|| vec![String::from("/dev/stdin")]),
            reverse: matches.get_flag("reverse"),
            key: if matches.get_flag("numeric") {
                SortKey::Numeric
            } else {
                SortKey::Lexical
            },
            notty: false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn defaults() {
        let command = cli();
        let matches = command.get_matches_from(["livesort"]);
        let config = Config::from(matches);
        assert_eq!(config.sources, vec!["/dev/stdin"]);
        assert!(!config.reverse);
        assert_eq!(config.key, SortKey::Lexical);
        assert_eq!(config.sources, vec!["/dev/stdin"]);
        assert!(!config.notty);
    }
}
