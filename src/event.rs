use crate::{complain, livesort};
use std::io;
use std::sync::atomic::Ordering::Relaxed;

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum MotionEvent {
    End,
    Home,
    LineDown,
    LineUp,
    PageDown,
    PageUp,
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum InputEvent {
    Motion(MotionEvent),
    Refresh,
    Invalid(u8),
    Quit,
}

/// Received a signal from the system
pub enum _SystemEvent {
    Interrupt, // Maybe also terminate, HUP, usr, etc
    WindowResize,
    Suspend,
    Continue,
}

// sources should probably have a type instead of a string identifier
#[derive(Debug)]
pub enum SourceEvent {
    LineRead {
        line: String,
    },
    SourceExhausted {
        source: String,
    },
    ReadError {
        source: String,
        error: std::io::Error,
    },
}

impl std::convert::From<u8> for InputEvent {
    fn from(value: u8) -> Self {
        use InputEvent::*;
        use MotionEvent::*;
        const ALT_V: u8 = b'\xF6';
        const ALT_B: u8 = b'\xE2';
        const ALT_F: u8 = b'\xE6';

        match value {
            b'' | b'b' | b'p' | b'k' | b'' => Motion(LineUp),
            b'' | b'f' | b'n' | b'j' | b'' => Motion(LineDown),
            b'' | b'' | ALT_V | ALT_B | b'K' | b'P' => Motion(PageUp),
            b' ' | b'' | ALT_F | b'J' | b'N' | b'F' => Motion(PageDown),
            b'q' | b'Q' | b'' => Quit,
            b'' | b'' => Refresh,
            b'<' => Motion(Home),
            b'>' => Motion(End),
            _ => Invalid(value),
        }
    }
}

pub trait EventHandler {
    fn handle(&self, app: &mut livesort::LiveSort) -> io::Result<()>;
}

impl EventHandler for MotionEvent {
    fn handle(&self, app: &mut livesort::LiveSort) -> io::Result<()> {
        use MotionEvent::*;
        let page = || -> usize { (app.height * 3) / 4 };
        let line_plus = |delta: isize| -> usize {
            let line = (app.cursor_line() as isize) + delta;
            line.clamp(0, app.lines.count() as isize) as usize
        };
        let line = match self {
            End => (app.lines.count() as isize - page() as isize).max(0) as usize,
            Home => 0,
            LineDown => line_plus(1),
            LineUp => line_plus(-1),
            PageDown => line_plus(page() as isize),
            PageUp => line_plus(-(page() as isize)),
        };
        app.set_cursor_line(line)
    }
}

impl EventHandler for InputEvent {
    fn handle(&self, app: &mut livesort::LiveSort) -> io::Result<()> {
        match self {
            InputEvent::Motion(motion) => motion.handle(app),
            InputEvent::Refresh => app.refresh_display(),
            InputEvent::Quit => {
                app.quitting.store(true, Relaxed);
                Ok(())
            }
            InputEvent::Invalid(_) => Ok(()),
        }
    }
}

impl EventHandler for SourceEvent {
    fn handle(&self, app: &mut livesort::LiveSort) -> io::Result<()> {
        match self {
            SourceEvent::LineRead { line } => {
                app.lines.index_line(line.to_string());
                app.display.update_display(app.calculate_display())?;
                app.update_status_line()
            }
            SourceEvent::SourceExhausted { source } => {
                app.active_sources.remove(source);
                if app.active_sources.is_empty() {
                    app.quitting.store(true, Relaxed);
                }
                Ok(())
            }
            SourceEvent::ReadError { source, error } => {
                Ok(complain(&format!("Read error on {}: {:?}", source, error)))
            }
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn from() {
        assert_eq!(
            InputEvent::Motion(MotionEvent::PageDown),
            InputEvent::from(b' ')
        );
    }
}
