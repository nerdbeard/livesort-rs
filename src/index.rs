/*!
TODO:
- Need to quickly access lines by index and get the total number
  of lines indexed; current methods are very expensive
- Replace get_all with an into_iter version that consumes self
*/

use crate::config::SortKey;
use std::{collections::BTreeMap, rc::Rc};

pub trait Index {
    fn index_line(&mut self, line: String);
    fn get_all(&self) -> Box<dyn Iterator<Item = &str> + '_>;
    fn get_range(&self, first: usize, count: usize) -> Box<dyn Iterator<Item = &str> + '_>;
    fn count(&self) -> usize;
}

pub fn factory(key: SortKey, reverse: bool) -> Box<dyn Index> {
    match key {
        SortKey::Numeric => Box::new(_Index::<NumericKey>::new(reverse)),
        SortKey::Lexical => Box::new(_Index::<LexicalKey>::new(reverse)),
    }
}

pub trait Key: Ord + From<Rc<String>> {}

#[derive(Ord, Eq, PartialEq, PartialOrd)]
pub struct LexicalKey(Rc<String>);
impl Key for LexicalKey {}
impl From<Rc<String>> for LexicalKey {
    fn from(value: Rc<String>) -> Self {
        LexicalKey(value)
    }
}

#[derive(Ord, Eq, PartialEq, PartialOrd)]
pub struct NumericKey(isize);
impl Key for NumericKey {}
impl From<Rc<String>> for NumericKey {
    fn from(value: Rc<String>) -> Self {
        Self(if let Some(value) = value.split_whitespace().next() {
            value.parse::<isize>().unwrap_or(isize::MIN)
        } else {
            isize::MIN
        })
    }
}

struct _Index<KeyT>
where
    for<'a> KeyT: Key,
{
    lines: BTreeMap<KeyT, Vec<Rc<String>>>,
    reverse: bool,
}

impl<KeyT: for<'a> Key> _Index<KeyT> {
    pub fn new(reverse: bool) -> Self {
        _Index {
            lines: BTreeMap::new(),
            reverse,
        }
    }
    fn get_lines(&self) -> Box<dyn Iterator<Item = &Rc<String>> + '_> {
        let lines = self.lines.values().flatten();
        let lines: Box<dyn Iterator<Item = &Rc<String>>> = match self.reverse {
            false => Box::new(lines),
            true => Box::new(lines.rev()),
        };
        lines
    }
}

impl<KeyT: for<'a> Key> Index for _Index<KeyT> {
    fn index_line(&mut self, line: String) {
        let line = Rc::new(line);
        let key = KeyT::from(Rc::clone(&line));
        match self.lines.get_mut(&key) {
            Some(bucket) => {
                bucket.push(line);
            }
            None => {
                self.lines.insert(key, vec![line]);
            }
        }
    }

    fn get_all(&self) -> Box<dyn Iterator<Item = &str> + '_> {
        Box::new(self.get_lines().map(|s| s.as_str()))
    }

    fn get_range(&self, first: usize, count: usize) -> Box<dyn Iterator<Item = &str> + '_> {
        Box::new(
            self.get_lines()
                .skip(first)
                .map(|s| s.as_str())
                .chain(std::iter::repeat(""))
                .take(count),
        )
    }

    fn count(&self) -> usize {
        self.get_all().count() // XXX: Expensive!
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn setup<T: for<'a> Key>() -> (_Index<T>, String) {
        let mut index = _Index::<T>::new(false);
        let line = String::from("a line");
        index.index_line(line.clone());
        (index, line)
    }

    #[test]
    fn index_line() {
        fn test<T: for<'a> Key>() {
            let (index, line) = setup::<T>();
            let lines: Vec<&Rc<String>> = index.lines.values().flatten().collect();
            assert_eq!(lines, vec![&Rc::new(line)]);
        }
        test::<LexicalKey>();
        test::<NumericKey>();
    }

    #[test]
    fn get_range() {
        let (mut index, line) = setup::<LexicalKey>();
        let lines = (
            line,
            String::from("second line"),
            String::from("third line"),
        );
        index.index_line(lines.1.clone());
        index.index_line(lines.2.clone());
        assert_eq!(
            index.get_range(0, 2).collect::<Vec<&str>>(),
            vec![&lines.0, &lines.1]
        );
        assert_eq!(
            index.get_range(1, 2).collect::<Vec<&str>>(),
            vec![&lines.1, &lines.2]
        );
    }

    #[test]
    fn factory() {
        crate::index::factory(SortKey::Lexical, false);
    }

    #[test]
    fn into_key() {
        let key: NumericKey = Rc::new(String::from("")).into();
        assert_eq!(isize::MIN, key.0);
    }
}
