* livesort-rs

** About
A replacement for `/bin/sort` that allows you to scroll/page through
the output it is going to produce as it is being calculated.  The
output is still produced to `stdout` like normal.  The interface uses
`/dev/tty` which (mostly) does not disturb the pipeline, allowing
`livesort` to take the place of `sort` and present a user interface
while it works.

The primary public source repository is [[https://codeberg.org/nerdbeard/livesort-rs][on Codeberg]].

** Example usage
#+begin_example
$ du /usr | livesort -rn | head -5
[ Livesort presents live interface, performs sort, then restores the
  terminal and outputs the results to head: ]
7489248 /usr/lib
4006168 /usr/lib/x86_64-linux-gnu
1361772 /usr/lib/i386-linux-gnu
856980  /usr/share/fonts
618224  /usr/share/doc
livesort: Error outputting results: Broken pipe (os error 32)
$ #fin
#+end_example

** Typical uses
Often, the intermediate results of a sort operation are good enough.
For example, when you are looking for large files to delete, you don't
necessarily need an exhaustive report, you are just interested in
finding any large files you don't want anymore so that you can quickly
free up space.  Piping `du` to `sort` won't give you any results until
the entire output of `du` has been delivered and processed.

** Key bindings

- Page down :: Space, `J`, `^V`
- Page up :: Backspace, `K`, Alt-`V`
- Line down :: Enter, `j`, `^N`
- Line up :: `k`, `^P`
- Home :: `<`
- End :: `>`
- Quit :: Esc, `q`

** Version
This is a PRE-ALPHA "release"!

** Dedication
Please be merciful and compassionate; do not consume or trade in
animal products.  Once we could say, "nature is cruel," and pretend
that was an excuse.  Today, cattle have more biomass than any other
animal on Earth.  Humanity is cruel.  But unlike nature, we can
change.  Please stop being cruel and violent and pretending it doesn't
happen.

** License
As the author and copyright holder, I am offering this software to the
general public under the [[file:gpl-3.0.txt][GPL3]].  Kisses!  --mike

** Todos
- notice when stdout is already a terminal, ie interactive mode; don't
  output final sorted result to stdout and wait for user to exit
- notice when stdin is a terminal and... I dunno!  Disable user input?
- write a better [[file:README.org][README.org file]]
- implement /bin/sort compatibility
- should never ever exit without cleaning up the terminal
- "stick" the cursor to its line rather than its absolute offset, so
  its line remains static on the screen
- cursor line should appear centred on the screen unless there are not
  enough lines to fill the screen with as many lines as possible while
  the cursor line is centred
- If you quit using 'Q' instead of '^C', the intermediate sorted
  output is sent to stdout.  Probably bad default behaviour!!
- The main loop is still not fully responsive when the source loop is
  slow.  I don't know why the main loop would ever block on the source
  loop but the interval of unresponsiveness increases with the delay
  between lines from the source stream.
- [[file:src/display.rs::// Erase the old status line][When erasing the status line]], draw the exposed characters that were
  underneath it.
